use common::detect_ecb;
use log::error;
use rustc_serialize::hex::FromHex;
use std::error::Error;
use std::io::{stdin, BufRead};

fn detect_aes_ecb(input: &mut dyn BufRead) -> Result<String, Box<dyn Error>> {
    for line in input.lines() {
        let line = line?;
        let ciphertext = line.from_hex()?;
        if detect_ecb(ciphertext.as_slice()) {
            return Ok(line);
        }
    }
    Err("No ECB encrypted ciphertext found".into())
}

fn main() {
    env_logger::init();

    match detect_aes_ecb(&mut stdin().lock()) {
        Ok(s) => println!("{}", s),
        Err(e) => error!("{}", e),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs::File;
    use std::io::BufReader;
    use std::path::Path;

    #[test]
    fn challenge8_detect_ecb() {
        let cargo_manifest_dir = Path::new(env!("CARGO_MANIFEST_DIR"));
        let testinput = File::open(cargo_manifest_dir.join("8.txt")).unwrap();
        assert_eq!(
            detect_aes_ecb(&mut BufReader::new(testinput)).unwrap(),
            "d880619740a8a19b7840a8a31c810a3d08649af70dc06f4fd5d2d69c744cd283e2dd052f6b641dbf9d11b0348542bb5708649af70dc06f4fd5d2d69c744cd2839475c9dfdbc1d46597949d9c7e82bf5a08649af70dc06f4fd5d2d69c744cd28397a93eab8d6aecd566489154789a6b0308649af70dc06f4fd5d2d69c744cd283d403180c98c8f6db1f2a3f9c4040deb0ab51b29933f2c123c58386b06fba186a"
        );
    }
}
