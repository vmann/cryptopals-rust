use common::break_xor_cipher;
use log::error;
use rustc_serialize::hex::FromHex;
use std::io::{stdin, BufRead};

fn main() {
    env_logger::init();

    for line in stdin().lock().lines().filter_map(|x| x.ok()) {
        match line.from_hex() {
            Ok(ciphertext) => {
                let (decrypted, _) = break_xor_cipher(ciphertext.as_slice());
                match String::from_utf8(decrypted) {
                    Ok(s) => println!("{}", s),
                    Err(e) => error!("{}", e),
                }
            }
            Err(e) => error!("{}", e),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn challenge3_xor_cipher() {
        let ciphertext = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736"
            .from_hex()
            .unwrap();
        let (decrypted, _) = break_xor_cipher(ciphertext.as_slice());
        assert_eq!(
            String::from_utf8(decrypted).unwrap(),
            "Cooking MC's like a pound of bacon"
        );
    }
}
