use common::break_repeating_xor_cipher;
use log::error;
use rustc_serialize::base64::FromBase64;
use std::error::Error;
use std::io::{read_to_string, stdin, Read};

fn break_xor(input: &mut dyn Read) -> Result<String, Box<dyn Error>> {
    let ciphertext = read_to_string(input)?.from_base64()?;
    let decrypted = break_repeating_xor_cipher(ciphertext.as_slice());
    Ok(String::from_utf8(decrypted)?)
}

fn main() {
    env_logger::init();

    match break_xor(&mut stdin().lock()) {
        Ok(s) => println!("{}", s),
        Err(e) => error!("{}", e),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs::File;
    use std::io::BufReader;
    use std::path::Path;

    #[test]
    fn challenge6_break_repeating_xor() {
        let cargo_manifest_dir = Path::new(env!("CARGO_MANIFEST_DIR"));
        let testinput = File::open(cargo_manifest_dir.join("6.txt")).unwrap();
        let decrypted = break_xor(&mut BufReader::new(testinput)).unwrap();
        let plaintext =
            read_to_string(File::open(cargo_manifest_dir.join("6_plaintext.txt")).unwrap())
                .unwrap();
        assert_eq!(decrypted, plaintext);
    }
}
