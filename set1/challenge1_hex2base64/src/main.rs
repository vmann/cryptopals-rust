use log::error;
use rustc_serialize::base64::{ToBase64, STANDARD};
use rustc_serialize::hex::FromHex;
use std::io::{stdin, BufRead};

fn main() {
    env_logger::init();

    for line in stdin().lock().lines().filter_map(|x| x.ok()) {
        match line.from_hex() {
            Ok(v) => println!("{}", v.to_base64(STANDARD)),
            Err(e) => error!("{}", e),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn challenge1_hex2base64() {
        let v = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d".from_hex().unwrap();
        let r = v.to_base64(STANDARD);
        assert_eq!(
            r,
            "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t"
        );
    }
}
