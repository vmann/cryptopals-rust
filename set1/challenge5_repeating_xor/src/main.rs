use common::repeating_xor_cipher;
use rustc_serialize::hex::ToHex;
use std::io::{stdin, BufRead};

fn main() {
    env_logger::init();

    for line in stdin().lock().lines().filter_map(|x| x.ok()) {
        let ciphertext = repeating_xor_cipher(line.as_bytes(), "ICE".as_bytes());
        println!("{}", ciphertext.to_hex());
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn challenge5_repeating_xor() {
        let plaintext =
            "Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal"
                .as_bytes();
        let ciphertext = repeating_xor_cipher(plaintext, "ICE".as_bytes());
        assert_eq!(ciphertext.to_hex(), "0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f");
    }
}
